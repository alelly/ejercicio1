

import 'bootstrap/dist/css/bootstrap.min.css';
import 'leaflet/dist/leaflet.css'
import './App.css';
import DatosTabla from './componentes/DatosTabla';
import NavbarInicio from './componentes/NavbarInicio.jsx';


function App() {

  return (
    <div className="App">
      <NavbarInicio />

      <DatosTabla />


    </div>
  );
}

export default App;
