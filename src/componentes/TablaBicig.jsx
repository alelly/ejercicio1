import { Table } from "react-bootstrap";


function TablaBicing(props) {

    let cabecera = props.head.map((elemento, i) => {
        return <th key={i}>{elemento}</th>
    })

    return (
        <div>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        {cabecera}
                    </tr>
                </thead>

                <tbody>
                    {props.body}
                </tbody>
            </Table>
        </div>
    );


}

export default TablaBicing;