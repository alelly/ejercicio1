
import { Navbar, Container } from 'react-bootstrap'
//import { Link } from 'react-router-dom';

function NavbarInicio() {

    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Container >
                    <Navbar.Brand href="#home">Bicing Table</Navbar.Brand>
                </Container>
            </Navbar>
        </div >

    )
}
export default NavbarInicio;
