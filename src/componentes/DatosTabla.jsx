import { useState } from 'react';
import { Button, Container } from 'react-bootstrap';
import Tabla from './TablaBicig';
import Mapa from './Mapa';

function DatosTabla() {
    const [data, setData] = useState([])
    const [filtro, setFiltro] = useState("")
    const header = ["Estación", "Bicis disponibles", "Slots Libres", "Latitud", "Longitud"]
    let posiciones = []

    function cargarDatos() {
        fetch("https://api.citybik.es/v2/networks/bicing")
            .then(response => response.json())
            .then(result => {
                //console.log("Funciona")
                if (filtro !== "") {
                    setData((result.network.stations).filter(bicis => bicis.free_bikes >= parseInt(filtro)))
                } else {
                    setData(result.network.stations)
                }
            })
            .catch((e) => console.log("Error: " + e))
    }
    // filtro automatico al escribir
    // useEffect(() => {
    //     cargarDatos()
    // }, [filtro])

    function llenarCampos(bici, i) {



        //console.log(data)
        return (
            <tr key={i}>
                <td>{bici.name}</td>
                <td>{bici.free_bikes}</td>
                <td>{bici.empty_slots}</td>
                <td>{bici.latitude}</td>
                <td>{bici.longitude}</td>
            </tr>
        );
    }
    let rows = data.map((bici, i) => llenarCampos(bici, i))
    //console.log(filtro)

    return (
        <div>
            <Container>
                {/* <div>
                    <Mapa data={posiciones} />
                </div> */}

                <Button className="boton" onClick={cargarDatos}>Cargar datos</Button>
                <input className='barrafiltro' type="text" placeholder="escribir datos" onInput={(e) => { setFiltro(e.target.value) }} />
                <Tabla head={header} body={rows} />
            </Container >
        </div >

    );

}
export default DatosTabla;